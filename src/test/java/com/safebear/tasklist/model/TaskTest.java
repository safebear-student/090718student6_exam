package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {

    // 10. Why is JUnit not picking up this test?
    public void creation(){

        LocalDate localDate = LocalDate.now();
        Task task = new Task(1L, "Mop the floors", localDate, false);

        // 13. Why is this assertion failing?
        Assertions.assertThat(task.getName()).isEqualTo(1L);

    }

}
